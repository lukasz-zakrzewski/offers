import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss']
})
export class StatusComponent {

  @Input() status: Number;

  private statuses = {
    '0': { text: '', color: 'black' },
    '1': { text: 'Wolne', color: 'green' },
    '2': { text: 'Rezerwacja', color: 'orange' },
    '3': { text: 'Sprzedane', color: 'red' },
    '4': { text: 'Przygotowany', color: 'green' },
    '5': { text: 'Przekazane', color: 'green' },
    '6': { text: 'Najem', color: 'blue' },
    '7': { text: 'Wstepnie zarezerwowany', color: 'blue' },
    '8': { text: 'Obserwowany', color: 'blue' },
    '9': { text: 'Zarezerwowane', color: 'orange' }
  };

  constructor() { }

  
}
