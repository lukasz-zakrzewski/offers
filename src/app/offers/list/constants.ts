export const investments = [
    { text: 'Wszystkie' },
    { text: 'Strumykowy Zakątek Sp. z o.o.', value: 'Strumykowy Zakątek Sp. z o.o.' },
    { text: 'Skorupska', value: 'Skorupska' },
    { text: 'Kawaleryjska', value: 'Kawaleryjska' },
    { text: 'Testy', value: 'Testy' }
];

export const columns = [
    { text: 'Inwestycja', value: 'name' },
    { text: 'Nr', value: 'nr' },
    { text: 'Piętro', value: 'floor' },
    { text: 'Powierzchnia', value: 'surface' },
    { text: 'Liczba pokoi', value: 'rooms' },
    { text: 'Status', value: 'status' },
    { text: 'Cena za m2', value: 'm2' },
    { text: 'Cena', value: 'price' }
];