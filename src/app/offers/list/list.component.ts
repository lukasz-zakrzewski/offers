import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { offers } from '../offers-data';
import { investments, columns } from './constants';
import { isUndefined } from 'util';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  private offers;
  private filteredOffers;
  private investments;
  private columns;

  private filters: any = {};
  private sort: any = {
    column: undefined,
    direction: 1
  };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.offers = [ ...offers ];
    this.investments = [ ...investments ];
    this.columns = [ ...columns ];
  }

  ngOnInit() {
    this.setFilters();
    this.filterOffers();
  }

  setFilters() {
    this.filters = {
      investment: this.route.snapshot.queryParams.investment,
      surfaceFrom: this.parseNumberOrUndefined(this.route.snapshot.queryParams.surfaceFrom),
      surfaceTo: this.parseNumberOrUndefined(this.route.snapshot.queryParams.surfaceTo),
      rooms: this.parseNumberOrUndefined(this.route.snapshot.queryParams.rooms),
      floor: this.parseNumberOrUndefined(this.route.snapshot.queryParams.floor)
    }
  }

  parseNumberOrUndefined(value) {
    const number = parseFloat(value);
    return isNaN(number) ? undefined : number;
  }

  searchClicked(){
    this.removeEmptyFilters();
    this.router.navigate(['offers', this.filters]);
    this.filterOffers();
  }

  removeEmptyFilters() {
    Object.keys(this.filters).forEach(key => {
      if(this.filters[key] === undefined)
        delete this.filters[key];
    });
  }

  isEmpty(value) {
    return value === null || value === undefined;
  }

  filterOffers() {
    this.filteredOffers = this.offers.filter(o => {
      const investment =  this.filters.investment === undefined || o.name === this.filters.investment;
      const surfaceFrom = this.filters.surfaceFrom === undefined || o.surface >= this.filters.surfaceFrom;
      const surfaceTo = this.filters.surfaceTo === undefined || o.surface <= this.filters.surfaceTo;
      const rooms = this.filters.rooms === undefined || o.rooms === this.filters.rooms;
      const floor = this.filters.floor === undefined || o.floor === this.filters.floor;
      return investment && surfaceFrom && surfaceTo && rooms && floor;
    });
  }

  columnClicked(column) {
    this.setSortColumn(column);
    this.sortOffers();
  }

  setSortColumn(column) {
    if(this.sort.column == column.value){
      this.sort.direction *= -1; 
    } else {
      this.sort.column = column.value;
      this.sort.direction = 1;
    }
  }

  sortOffers() {
    this.filteredOffers.sort((a, b) => {
      if(a[this.sort.column] === b[this.sort.column]) return 0;
      if(a[this.sort.column] > b[this.sort.column])
        return this.sort.direction;
      else 
        return this.sort.direction * -1;
    });
  }

}
