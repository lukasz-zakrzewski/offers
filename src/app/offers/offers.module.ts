import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { OffersRoutingModule } from './offers-routing.module';
import { ListComponent } from './list/list.component';
import { StatusComponent } from './status/status.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    
    OffersRoutingModule
  ],
  declarations: [ListComponent, StatusComponent],
  exports: [ListComponent]
})
export class OffersModule { }
